﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Test.WebApi.RegistrationService.Identity.Entities;
using Test.WebApi.RegistrationService.Identity.Extentions;
using Test.WebApi.RegistrationService.Identity.Managers;
using Test.WebApi.RegistrationService.Identity.Managers.Fabrics;
using Test.WebApi.RegistrationService.Identity.Stores.UserStores;
using static Test.WebApi.RegistrationService.Models.ViewModels.AccountBindingViewModels;

namespace Test.WebApi.RegistrationService.Controllers
{
	[Authorize]
	[RoutePrefix("api/Account")]
	public class AccountController : ApiController
	{
		#region Private fields
		private readonly UserManager<CustomIdentityUser, Guid> _userManager;
		#endregion

		#region Constructors
		public UserManager<CustomIdentityUser, Guid> UserManager { get { return _userManager; } }

		public AccountController(CustomUserManager userManager)
		{
			_userManager = userManager;
		}
		#endregion

		[AllowAnonymous]
		[Route("Register")]
		public async Task<IHttpActionResult> Register(RegisterBindingModel model)
		{
			if (!ModelState.IsValid) { return BadRequest(ModelState); }
			CustomIdentityUser user = model.Map();
			IdentityResult result = await UserManager.CreateAsync(user, model.Password);
			return !result.Succeeded ? GetErrorResult(result) : Ok();
		}

		#region Private Helpers
		private IHttpActionResult GetErrorResult(IdentityResult result)
		{
			if (result == null) { return InternalServerError(); }
			if (result.Succeeded) return null;
			if (result.Errors != null)
			{
				foreach (string error in result.Errors) { ModelState.AddModelError("", error); }
			}
			if (ModelState.IsValid)
			{
				// No ModelState errors are available to send, so just return an empty BadRequest.
				return BadRequest();
			}
			return BadRequest(ModelState);
		} 
		#endregion
	}
}
