﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.WebApi.RegistrationService.Identity.Entities;
using System.Threading.Tasks;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data.Entity.SqlServer.Utilities;
using System.Net.Mail;

namespace Test.WebApi.RegistrationService.Identity.Validators
{
	public class CustomEmailValidator : IIdentityValidator<CustomIdentityUser>
	{
		public bool AllowOnlyAlphanumericUserNames { get; set; }
		public bool RequireUniqueEmail { get; set; }

		private UserManager<CustomIdentityUser, Guid> Manager { get; set; }

		public CustomEmailValidator(UserManager<CustomIdentityUser, Guid> manager)
		{
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}
			AllowOnlyAlphanumericUserNames = true;
			Manager = manager;
		}

		public Task<IdentityResult> ValidateAsync(CustomIdentityUser item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}
			var errors = new List<string>();
			ValidateUserName(item, errors);
			if (RequireUniqueEmail)
			{
				ValidateEmailAsync(item, errors);
			}
			if (errors.Count > 0)
			{
				return Task.FromResult(IdentityResult.Failed(errors.ToArray()));
			}
			return Task.FromResult(IdentityResult.Success);
		}

		private void ValidateEmailAsync(CustomIdentityUser user, List<string> errors)
		{
			var email = user.Email;
			if (string.IsNullOrWhiteSpace(email))
			{
				errors.Add(String.Format(CultureInfo.CurrentCulture, "Email"));
				return;
			}
			try
			{
				var m = new MailAddress(email);
			}
			catch (FormatException)
			{
				errors.Add(String.Format(CultureInfo.CurrentCulture, email));
				return;
			}
			var owner = Manager.FindByEmailAsync(email).WithCurrentCulture().GetResult();
			if (owner != null && !EqualityComparer<Guid>.Default.Equals(owner.Id, user.Id))
			{
				errors.Add(String.Format(CultureInfo.CurrentCulture, email));
			}
		}

		private void ValidateUserName(CustomIdentityUser user, List<string> errors)
		{
			if (string.IsNullOrWhiteSpace(user.UserName))
			{
				errors.Add(String.Format(CultureInfo.CurrentCulture, "Name"));
			}
			else if (AllowOnlyAlphanumericUserNames && !Regex.IsMatch(user.UserName, @"^[A-Za-z0-9@_\.]+$"))
			{
				// If any characters are not letters or digits, its an illegal user name
				errors.Add(String.Format(CultureInfo.CurrentCulture, user.UserName));
			}
			else
			{
				var owner = Manager.FindByNameAsync(user.UserName).WithCurrentCulture().GetResult();
				if (owner != null && !EqualityComparer<Guid>.Default.Equals(owner.Id, user.Id))
				{
					errors.Add(String.Format(CultureInfo.CurrentCulture, user.UserName));
				}
			}
		}
	}
}