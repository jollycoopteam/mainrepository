﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Test.WebApi.RegistrationService.Identity.Entities;
using System.Threading.Tasks;
using Test.WebApi.RegistrationService.Identity.Extentions;

namespace Test.WebApi.RegistrationService.Identity.Stores.UserStores.AppUserStore
{
	public partial class AppUserStore : IUserEmailStore<CustomIdentityUser, Guid>
	{
		public Task SetEmailAsync(CustomIdentityUser user, string email)
		{
			throw new NotImplementedException();
		}

		public Task<string> GetEmailAsync(CustomIdentityUser user)
		{
			return Task.FromResult(user.Email);
		}

		public Task<bool> GetEmailConfirmedAsync(CustomIdentityUser user)
		{
			throw new NotImplementedException();
		}

		public Task SetEmailConfirmedAsync(CustomIdentityUser user, bool confirmed)
		{
			throw new NotImplementedException();
		}

		public Task<CustomIdentityUser> FindByEmailAsync(string email)
		{
			email.ThrowIfDefault();
			var dbUser = _uOw.UserRepository.FindByEmail(email);
			return Task.FromResult(dbUser.Map());
		}
	}
}