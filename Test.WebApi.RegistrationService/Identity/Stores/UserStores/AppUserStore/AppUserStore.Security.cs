﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Test.WebApi.RegistrationService.Identity.Entities;
using System.Threading.Tasks;
using Test.WebApi.RegistrationService.Identity.Extentions;

namespace Test.WebApi.RegistrationService.Identity.Stores.UserStores.AppUserStore
{
	public partial class AppUserStore : IUserSecurityStampStore<CustomIdentityUser, Guid>
	{
		public Task SetSecurityStampAsync(CustomIdentityUser user, string stamp)
		{
			user.ThrowIfNull();
			stamp.ThrowIfDefault();
			user.SecurityStamp = stamp;
			return Task.FromResult(0);
		}

		public Task<string> GetSecurityStampAsync(CustomIdentityUser user)
		{
			return Task.FromResult(user.SecurityStamp);
		}
	}
}