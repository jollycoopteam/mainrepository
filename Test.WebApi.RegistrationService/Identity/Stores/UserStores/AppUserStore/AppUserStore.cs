﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Test.WebApi.RegistrationService.Identity.Entities;
using System.Threading.Tasks;
using Backend.Data.EF.Abstractions;
using Test.WebApi.RegistrationService.Identity.Extentions;

namespace Test.WebApi.RegistrationService.Identity.Stores.UserStores.AppUserStore
{
	public partial class AppUserStore : IUserStore<CustomIdentityUser, Guid>
	{
		private readonly IUnitOfWork _uOw;

		public AppUserStore(IUnitOfWork uOw)
		{
			_uOw = uOw;
		}
		public void Dispose(){}

		public Task CreateAsync(CustomIdentityUser user)
		{
			user.ThrowIfNull();
			var dbUser = user.Map();
			_uOw.UserRepository.Add(dbUser);
			return _uOw.SaveChangesAsync();
		}
		/// <summary>
		/// !volatile method.
		/// </summary>
		/// <param name="user"></param>
		/// <returns></returns>
		public Task UpdateAsync(CustomIdentityUser user)
		{
			user.ThrowIfNull();
			var dbUser = user.Map();
			_uOw.UserRepository.Update(dbUser);
			return _uOw.SaveChangesAsync();
		}

		public Task DeleteAsync(CustomIdentityUser user)
		{
			user.ThrowIfNull();
			var dbUser = user.Map();
			_uOw.UserRepository.Remove(dbUser);
			return _uOw.SaveChangesAsync();
		}

		public Task<CustomIdentityUser> FindByIdAsync(Guid userId)
		{
			userId.ThrowIfDefault();
			var dbUser = _uOw.UserRepository.FindById(userId);
			return Task.FromResult(dbUser.Map());
		}

		public Task<CustomIdentityUser> FindByNameAsync(string userName)
		{
			userName.ThrowIfDefault();
			var dbUser = _uOw.UserRepository.FindByUserName(userName);
			return Task.FromResult(dbUser.Map());
		}
	}
}