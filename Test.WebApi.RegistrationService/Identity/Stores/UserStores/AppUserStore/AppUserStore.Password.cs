﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Test.WebApi.RegistrationService.Identity.Entities;
using System.Threading.Tasks;

namespace Test.WebApi.RegistrationService.Identity.Stores.UserStores.AppUserStore
{
	public partial class AppUserStore : IUserPasswordStore<CustomIdentityUser, Guid>
	{
		public Task SetPasswordHashAsync(CustomIdentityUser user, string passwordHash)
		{
			user.PasswordHash = passwordHash;
			return Task.FromResult(0);
		}

		public Task<string> GetPasswordHashAsync(CustomIdentityUser user)
		{
			return Task.FromResult(user.PasswordHash);
		}

		public Task<bool> HasPasswordAsync(CustomIdentityUser user)
		{
			throw new NotImplementedException();
		}
	}
}