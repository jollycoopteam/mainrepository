﻿using Backend.Domain.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Test.WebApi.RegistrationService.Identity.Entities;
using Backend.Data.EF;
using System.Data.Entity;

namespace Test.WebApi.RegistrationService.Identity.Stores
{
	/// <summary>
	/// Manages roles
	/// </summary>
	public class CustomRoleStore : IRoleStore<CustomIdentityRole, Guid>, IDisposable
	{
		private readonly ApplicationDbContext _ctx;

		public CustomRoleStore(ApplicationDbContext ctx)
		{
			this._ctx = ctx;
		}
		/// <summary>
		/// Create new Role
		/// </summary>
		/// <param name="role">role to create</param>
		/// <returns></returns>
		public Task CreateAsync(CustomIdentityRole role)
		{
			var dbRole = MapRole(role);
			_ctx.Roles.Add(dbRole);
			return _ctx.SaveChangesAsync();
		}
		/// <summary>
		/// Delete existing role
		/// </summary>
		/// <param name="role">role to delete</param>
		/// <returns></returns>
		public Task DeleteAsync(CustomIdentityRole role)
		{
			var dbRole = MapRole(role);
			_ctx.Roles.Remove(dbRole);
			return _ctx.SaveChangesAsync();
		}

		public void Dispose()
		{
		}
		/// <summary>
		/// Find role by specific id
		/// </summary>
		/// <param name="roleId">role id</param>
		/// <returns></returns>
		public Task<CustomIdentityRole> FindByIdAsync(Guid roleId)
		{
			var role = _ctx.Roles.Find(roleId);
			return Task.FromResult<CustomIdentityRole>(MapRole(role));
		}
		/// <summary>
		/// Find Role by specific name
		/// </summary>
		/// <param name="roleName">role name</param>
		/// <returns></returns>
		public Task<CustomIdentityRole> FindByNameAsync(string roleName)
		{
			var role = _ctx.Roles.FirstOrDefault(r => r.Name == roleName);
			return Task.FromResult<CustomIdentityRole>(MapRole(role));
		}
		/// <summary>
		/// Update role
		/// </summary>
		/// <param name="role">role to update</param>
		/// <returns></returns>
		public Task UpdateAsync(CustomIdentityRole role)
		{
			var roleToUpdate = MapRole(role);
			var entry = _ctx.Entry(roleToUpdate);
			if (entry.State == EntityState.Detached)
			{
				_ctx.Roles.Attach(roleToUpdate);
				entry = _ctx.Entry(roleToUpdate);
			}
			entry.State = EntityState.Modified;
			return _ctx.SaveChangesAsync();
		}

		private Role MapRole(CustomIdentityRole role)
		{
			return new Role
			{
				RoleId = role.Id,
				Name = role.Name
			};
		}
		private CustomIdentityRole MapRole(Role role)
		{
			return new CustomIdentityRole
			{
				Id = role.RoleId,
				Name = role.Name
			};
		}
	}
}