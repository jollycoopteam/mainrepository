﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.WebApi.RegistrationService.Identity.Entities
{
	public class CustomIdentityRole : IRole<Guid>
	{
		public CustomIdentityRole()
		{
			this.Id = Guid.NewGuid();
		}

		public CustomIdentityRole(string name)
			: this()
		{
			this.Name = name;
		}

		public CustomIdentityRole(string name, Guid id)
		{
			this.Name = name;
			this.Id = id;
		}

		public Guid Id { get; set; }
		public string Name { get; set; }
	}
}