﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Test.WebApi.RegistrationService.Identity.Entities
{
	public class CustomIdentityUser : IUser<Guid>
	{
		public CustomIdentityUser()
		{
			this.Id = Guid.NewGuid();
		}
		public CustomIdentityUser(string username){}
		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<CustomIdentityUser, Guid> manager, string authenticationType)
		{
			// Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
			var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
			// Add custom user claims here
			return userIdentity;
		}

		public Guid Id { get; set; }
		public string UserName { get; set; }
		public virtual string PasswordHash { get; set; }
		public virtual string SecurityStamp { get; set; }

		#region Opt Scalar Properties
		public virtual string Email { get; set; }
		public virtual bool EmailConfirmed { get; set; }
		public virtual string PhoneNumber { get; set; }
		public virtual bool PhoneNumberConfirmed { get; set; }
		public virtual bool TwoFactorEnabled { get; set; }

		public virtual int AccessFailedCount { get; set; }
		public virtual bool LockoutEnabled { get; set; }
		public virtual DateTime? LockoutEndDateUtc { get; set; }
		#endregion
	}
}