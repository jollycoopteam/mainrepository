﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.WebApi.RegistrationService.Identity.Entities;

namespace Test.WebApi.RegistrationService.Identity.Managers
{
	public class CustomRoleManager : RoleManager<CustomIdentityRole, Guid>
	{
		public CustomRoleManager(IRoleStore<CustomIdentityRole, Guid> roleStore)
			:base(roleStore)
		{
		}
	}
}