﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Test.WebApi.RegistrationService.Identity.Entities;

namespace Test.WebApi.RegistrationService.Identity.Managers
{
	public class CustomSignInManager : SignInManager<CustomIdentityUser, Guid>
	{
		public CustomSignInManager(UserManager<CustomIdentityUser, Guid> userManager, 
			IAuthenticationManager authenticationManager) : 
			base(userManager, authenticationManager)
		{

		}
	}
}