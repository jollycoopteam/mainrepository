﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Backend.Data.EF;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Test.WebApi.RegistrationService.Identity.Entities;
using Test.WebApi.RegistrationService.Identity.Stores.UserStores.AppUserStore;

namespace Test.WebApi.RegistrationService.Identity.Managers.Fabrics
{
	public class UserManagerFabric : IManagerFabric<UserManager<CustomIdentityUser, Guid>>
	{
		private readonly string _connStr;

		public UserManagerFabric()
		{
			_connStr = ConfigurationManager.ConnectionStrings["AuthServerDataBase"].ConnectionString;
		}
		public UserManager<CustomIdentityUser, Guid> CreateManager()
		{
			return new CustomUserManager(new AppUserStore(new UnitOfWork(_connStr)), new IdentityFactoryOptions<CustomUserManager>());
		}
	}
}