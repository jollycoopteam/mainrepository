﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Test.WebApi.RegistrationService.Identity.Entities;

namespace Test.WebApi.RegistrationService.Identity.Managers.Fabrics
{
	public interface IManagerFabric<T> where T : class 
	{
		T CreateManager();
	}
}
