﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Test.WebApi.RegistrationService.Identity.Entities;
using Test.WebApi.RegistrationService.Services;
using System.Threading.Tasks;
using Backend.Data.EF;
using Test.WebApi.RegistrationService.Identity.Stores.UserStores.AppUserStore;

namespace Test.WebApi.RegistrationService.Identity.Managers
{
	public sealed class CustomUserManager : UserManager<CustomIdentityUser, Guid>
	{
		public CustomUserManager(IUserStore<CustomIdentityUser, Guid> userStore, IdentityFactoryOptions<CustomUserManager> options)
			:base(userStore)
		{
			#region Validators
			this.UserValidator = new UserValidator<CustomIdentityUser, Guid>(this)
			{
				AllowOnlyAlphanumericUserNames = true,
				RequireUniqueEmail = true
			};
			this.PasswordValidator = new PasswordValidator
			{
				RequiredLength = 8,
				RequireDigit = true,
				RequireNonLetterOrDigit = false,
				RequireLowercase = true,
				RequireUppercase = false
			}; 
			#endregion
			this.UserLockoutEnabledByDefault = false;
			this.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
			this.MaxFailedAccessAttemptsBeforeLockout = 5;
			this.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<CustomIdentityUser, Guid>
			{
				MessageFormat = "Your security code is {0}"
			});
			this.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<CustomIdentityUser, Guid>
			{
				Subject = "Security Code",
				BodyFormat = "Your security code is {0}"
			});
			#region Email and Sms Services
			this.EmailService = new EmailService();
			this.SmsService = new SmsService(); 
			#endregion
			var dataProtectionProvider = options.DataProtectionProvider;
			if (dataProtectionProvider != null)
			{
				this.UserTokenProvider =
					new DataProtectorTokenProvider<CustomIdentityUser, Guid>(dataProtectionProvider.Create("ASP.NET Identity"));
			}
			this.PasswordHasher = new PasswordHasher();
		}
		public override Task<IdentityResult> CreateAsync(CustomIdentityUser user, string password)
		{
			return base.CreateAsync(user, password);
		}

		public override Task<CustomIdentityUser> FindAsync(string userName, string password)
		{
			return base.FindAsync(userName, password);
		}

		public static CustomUserManager Create()
		{
			var _connStr = ConfigurationManager.ConnectionStrings["AuthServerDataBase"].ConnectionString;
			return new CustomUserManager(new AppUserStore(new UnitOfWork(_connStr)), new IdentityFactoryOptions<CustomUserManager>());
		}
	}
}