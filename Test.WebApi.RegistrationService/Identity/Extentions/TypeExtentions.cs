﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.WebApi.RegistrationService.Identity.Extentions
{
	public static class TypeExtentions
	{
		public static void ThrowIfDefault(this Guid id)
		{
			if (id == default(Guid))
			{
				throw new ArgumentException("id");
			}
		}

		public static void ThrowIfDefault(this string id)
		{
			if (id == String.Empty)
			{
				throw new ArgumentException("id");
			}
		}
	}
}