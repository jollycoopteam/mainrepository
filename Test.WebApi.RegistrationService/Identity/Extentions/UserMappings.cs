﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Backend.Domain.Models;
using Test.WebApi.RegistrationService.Identity.Entities;
using Test.WebApi.RegistrationService.Models.ViewModels;

namespace Test.WebApi.RegistrationService.Identity.Extentions
{
	public static class UserMappings
	{
		public static CustomIdentityUser Map( this AccountBindingViewModels.RegisterBindingModel model)
		{
			return new CustomIdentityUser() { UserName = model.UserName, Email = model.UserEmail };
		}

		public static User Map(this CustomIdentityUser identity)
		{
			if (identity == null) return null;
			//identity.ThrowIfNull();
			return new User()
			{
				UserId = identity.Id,
				UserName = identity.UserName,
				PasswordHash = identity.PasswordHash,
				SecurityStamp = identity.SecurityStamp,
				Email = identity.Email,
				EmailConfirmed = identity.EmailConfirmed,
				PhoneNumber = identity.PhoneNumber,
				PhoneNumberConfirmed = identity.PhoneNumberConfirmed,
				AccessFailedCount = identity.AccessFailedCount,
				TwoFactorEnabled = identity.TwoFactorEnabled,
				LockoutEnabled = identity.LockoutEnabled,
				LockoutEndDateUtc = identity.LockoutEndDateUtc
			};
		}

		public static CustomIdentityUser Map(this User user)
		{
			if (user == null) return null;
			//user.ThrowIfNull();
			return new CustomIdentityUser()
			{
				Id = user.UserId,
				UserName = user.UserName,
				PasswordHash = user.PasswordHash,
				SecurityStamp = user.SecurityStamp,
				Email = user.Email,
				EmailConfirmed = user.EmailConfirmed,
				PhoneNumber = user.PhoneNumber,
				PhoneNumberConfirmed = user.PhoneNumberConfirmed,
				AccessFailedCount = user.AccessFailedCount,
				TwoFactorEnabled = user.TwoFactorEnabled,
				LockoutEnabled = user.LockoutEnabled,
				LockoutEndDateUtc = user.LockoutEndDateUtc
			};
		}

		// ReSharper disable once MemberCanBePrivate.Global
		internal static void ThrowIfNull<T>(this T user) where T : class 
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
		}

	}
}