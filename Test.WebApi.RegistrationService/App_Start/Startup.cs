﻿using Autofac.Integration.WebApi;
using Backend.Data.EF;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Test.WebApi.RegistrationService.Identity.Managers;
using Test.WebApi.RegistrationService.Identity.Managers.Fabrics;

[assembly: OwinStartup(typeof(Test.WebApi.RegistrationService.App_Start.Startup), "Configuration")]
namespace Test.WebApi.RegistrationService.App_Start
{
	public partial class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			var config = new HttpConfiguration();
			//InitializeDatabase();
			//app.CreatePerOwinContext<CustomUserManager>(CustomUserManager.Create);
			ConfigureWebApi(config);
			ConfigureAuth(app);
			var container = BuildContainer();
			config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
			app.UseAutofacMiddleware(container);
			app.UseAutofacWebApi(config);
			app.UseWebApi(config);
			app.UseWelcomePage();
		}

		private static void InitializeDatabase()
		{
			using (var db = new ApplicationDbContext())
			{
				db.Database.Initialize(true);
			}
		}
	}
}