﻿using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Test.WebApi.RegistrationService.App_Start
{
	public partial class Startup
	{
		private void ConfigureWebApi(HttpConfiguration config)
		{
			// Web API configuration and services
			// Configure Web API to use only bearer token authentication.
			config.SuppressDefaultHostAuthentication();
			config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
			RegisterRoutes(config);

			//Enable CORS for all origins, all headers, and all methods
			//You can customize this to match your requirments
			var cors = new EnableCorsAttribute("*", "*", "*");
			config.EnableCors(cors);
		}

		private void RegisterRoutes(HttpConfiguration config)
		{
			// route-based configuration
			config.MapHttpAttributeRoutes();
			// convention-based configuration.
			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{action}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);

			var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
			jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
		}
	}
}