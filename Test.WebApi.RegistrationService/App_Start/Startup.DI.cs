﻿using Autofac;
using Autofac.Integration.WebApi;
using Backend.Data.EF;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Backend.Data.EF.Abstractions;
using Microsoft.Owin.Security.DataHandler.Serializer;
using Microsoft.Owin.Security.DataProtection;
using Test.WebApi.RegistrationService.Controllers;
using Test.WebApi.RegistrationService.Identity.Entities;
using Test.WebApi.RegistrationService.Identity.Managers;
using Test.WebApi.RegistrationService.Identity.Stores;
using Test.WebApi.RegistrationService.Identity.Stores.UserStores;
using Test.WebApi.RegistrationService.Identity.Stores.UserStores.AppUserStore;

namespace Test.WebApi.RegistrationService.App_Start
{
	public partial class Startup
	{
		private static IContainer BuildContainer()
		{
			var builder = new ContainerBuilder();
			WireUp(builder);
			return builder.Build();
		}
		/// <summary>
		/// Method-wrapper for IoC container registration and resolving dependencies
		/// </summary>
		/// <param name="builder"></param>
		private static void WireUp(ContainerBuilder builder)
		{
			builder.RegisterType<ApplicationDbContext>().AsSelf().InstancePerRequest();
			builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
			builder.RegisterType<AppUserStore>().As<IUserStore<CustomIdentityUser, Guid>>().InstancePerRequest();
			builder.Register<IdentityFactoryOptions<CustomUserManager>>(c => new IdentityFactoryOptions<CustomUserManager>()
			{
				DataProtectionProvider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("RegistrationService")
			});
			builder.RegisterType<CustomUserManager>().AsSelf().InstancePerRequest();
			builder.RegisterType<AccountController>().AsSelf().InstancePerRequest();
		}
	}
}