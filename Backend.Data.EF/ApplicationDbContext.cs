﻿using Backend.Data.EF.Configuration;
using Backend.Data.EF.Configuration.Initializers;
using Backend.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Data.EF
{
	public class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext(string nameOrConnString) : base(nameOrConnString)
		{
			Database.SetInitializer<ApplicationDbContext>(new IfNotExistsInitializer());
		}
		public ApplicationDbContext() : base("name=AuthServerDataBase")
		{
			Database.SetInitializer<ApplicationDbContext>(new IfNotExistsInitializer());
		}
		public IDbSet<User> Users { get; set; }
		public IDbSet<Role> Roles { get; set; }
		public IDbSet<ExternalLogin> Logins { get; set; }
		public IDbSet<Claim> Claims { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new UserConfiguration());
			modelBuilder.Configurations.Add(new RoleConfiguration());
			modelBuilder.Configurations.Add(new ExternalLoginConfiguration());
			modelBuilder.Configurations.Add(new ClaimConfiguration());
		}
	}
}
