﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using Backend.Domain.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Data.EF.Configuration
{
	internal class RoleConfiguration : EntityTypeConfiguration<Role>
	{
		internal RoleConfiguration()
		{
			ToTable("Roles");

			HasKey(x => x.RoleId)
				.Property(x => x.RoleId)
				.HasColumnName("RoleId")
				.HasColumnType("uniqueidentifier")
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
				.IsRequired();

			Property(x => x.Name)
				.HasColumnName("Name")
				.HasColumnType("nvarchar")
				.HasMaxLength(256)
				.IsRequired();
			/*Many to many relationship*/
			HasMany(x => x.Users)
				.WithMany(x => x.Roles)
				.Map(x =>
				{
					x.ToTable("UserRoles");
					x.MapLeftKey("RoleId");
					x.MapRightKey("UserId");
				});
		}
	}
}
