﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using Backend.Domain.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Data.EF.Configuration
{
	internal class UserConfiguration : EntityTypeConfiguration<User>
	{
		internal UserConfiguration()
		{
			ToTable("Users");
			#region ScalarProperties Mappings
			// UserId
			HasKey(x => x.UserId)
				.Property(x => x.UserId)
				.HasColumnName("UserId")
				.HasColumnType("uniqueidentifier")
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
				.IsRequired();
			// UserName
			Property(x => x.UserName)
				.HasColumnName("UserName")
				.HasColumnType("nvarchar")
				.HasMaxLength(256)
				.IsRequired();
			// PasswordHash
			Property(x => x.PasswordHash)
			   .HasColumnName("PasswordHash")
			   .HasColumnType("nvarchar")
			   .IsMaxLength()
			   .IsOptional();
			// SecurityStamp
			Property(x => x.SecurityStamp)
				.HasColumnName("SecurityStamp")
				.HasColumnType("nvarchar")
				.IsMaxLength()
				.IsOptional();
			#endregion
			#region Opt Scalar Properties Mappings
			// Email
			Property(x => x.Email)
				.HasColumnName("Email")
				.HasColumnType("nvarchar")
				.HasMaxLength(256)
				.IsRequired();
			// EmailConfirmed
			Property(x => x.EmailConfirmed)
				.HasColumnName("EmailConfirmed")
				.HasColumnType("bit")
				.IsRequired();
			// PhoneNumber
			Property(x => x.PhoneNumber)
				.HasColumnName("PhoneNumber")
				.HasColumnType("nvarchar")
				.IsMaxLength()
				.IsOptional();
			// PhoneNumberConfirmed
			Property(x => x.PhoneNumberConfirmed)
				.HasColumnName("PhoneNumberConfirmed")
				.HasColumnType("bit")
				.IsRequired();
			// TwoFactorEnabled
			Property(x => x.TwoFactorEnabled)
				.HasColumnName("TwoFactorEnabled")
				.HasColumnType("bit")
				.IsRequired();
			// AccessFailedCount
			Property(x => x.AccessFailedCount)
				.HasColumnName("AccessFailedCount")
				.HasColumnType("int")
				.IsRequired();
			// LockoutEnabled
			Property(x => x.LockoutEnabled)
				.HasColumnName("LockoutEnabled")
				.HasColumnType("bit")
				.IsRequired();
			// LockoutEndDateUtc
			Property(x => x.LockoutEndDateUtc)
				.HasColumnName("LockoutEndDateUtc")
				.HasColumnType("datetime")
				.IsOptional();
			#endregion
			#region Navigation Properties Mappings
			// Roles
			HasMany(x => x.Roles)
				.WithMany(x => x.Users)
				.Map(x =>
				{
					x.ToTable("UserRoles");
					x.MapLeftKey("RoleId");
					x.MapRightKey("UserId");
				});
			// Claims
			HasMany(x => x.Claims)
				.WithRequired(x => x.User)
				.HasForeignKey(x => x.UserId);
			// Logins
			HasMany(x => x.Logins)
				.WithRequired(x => x.User)
				.HasForeignKey(x => x.UserId); 
			#endregion
		}
	}
}
