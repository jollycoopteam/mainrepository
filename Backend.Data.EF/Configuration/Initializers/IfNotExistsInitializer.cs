﻿using Backend.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Data.EF.Configuration.Initializers
{
	public class IfNotExistsInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
	{
		protected override void Seed(ApplicationDbContext context)
		{
			var roles = new List<Role>
			{
				new Role() { Name = "User" },
				new Role() { Name = "Admin" }
			};

			foreach (var role in roles)
			{
				context.Roles.Add(role);
			}
			base.Seed(context);
		}
	}
}
