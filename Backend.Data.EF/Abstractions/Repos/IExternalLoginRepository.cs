﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Backend.Domain.Models;

namespace Backend.Data.EF.Abstractions.Repos
{
	public interface IExternalLoginRepository : IGenericRepository<ExternalLogin, Guid>
	{
		ExternalLogin GetByProviderAndKey(string loginProvider, string providerKey);
		Task<ExternalLogin> GetByProviderAndKeyAsync(string loginProvider, string providerKey);
		Task<ExternalLogin> GetByProviderAndKeyAsync(CancellationToken cancellationToken, string loginProvider, string providerKey);
	}
}
