﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Backend.Domain.Models;

namespace Backend.Data.EF.Abstractions.Repos
{
	public interface IUserRepository : IGenericRepository<User, Guid>
	{
		User FindByUserName(string username);
		Task<User> FindByUserNameAsync(string username);
		Task<User> FindByUserNameAsync(CancellationToken cancellationToken, string username);

		User FindByEmail(string email);
		Task<User> FindByEmailAsync(string email);
		Task<User> FindByEmailAsync(CancellationToken cancellationToken, string email);
	}
}
