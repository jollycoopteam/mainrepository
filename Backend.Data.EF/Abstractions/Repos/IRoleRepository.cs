﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Backend.Domain.Models;

namespace Backend.Data.EF.Abstractions.Repos
{
	public interface IRoleRepository : IGenericRepository<Role, Guid>
	{
		Role FindByName(string roleName);
		Task<Role> FindByNameAsync(string roleName);
		Task<Role> FindByNameAsync(CancellationToken cancellationToken, string roleName);
	}
}
