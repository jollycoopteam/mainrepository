﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Backend.Data.EF.Abstractions.Repos
{
	public interface IGenericRepository<TEntity, Uid> where TEntity : class where Uid : struct 
	{
		List<TEntity> GetAll();
		Task<List<TEntity>> GetAllAsync();
		Task<List<TEntity>> GetAllAsync(CancellationToken cancellationToken);

		List<TEntity> PageAll(int skip, int take);
		Task<List<TEntity>> PageAllAsync(int skip, int take);
		Task<List<TEntity>> PageAllAsync(CancellationToken cancellationToken, int skip, int take);

		TEntity FindById(Uid id);
		Task<TEntity> FindByIdAsync(Uid id);
		Task<TEntity> FindByIdAsync(CancellationToken cancellationToken, Uid id);

		void Add(TEntity entity);
		void Update(TEntity entity);
		void Remove(TEntity entity);
	}
}
