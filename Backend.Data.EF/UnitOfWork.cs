﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Backend.Data.EF.Abstractions;
using Backend.Data.EF.Abstractions.Repos;
using Backend.Data.EF.Implementation.Repos;

namespace Backend.Data.EF
{
	public class UnitOfWork : IUnitOfWork
	{
		#region Fields
		private bool disposed = false;
		private readonly ApplicationDbContext _cxt;
		private IExternalLoginRepository _externalLoginRepository;
		private IRoleRepository _roleRepository;
		private IUserRepository _userRepository;
		#endregion

		public UnitOfWork()
		{
				_cxt = new ApplicationDbContext();
		}
		public UnitOfWork(string nameOrConnString)
		{
			_cxt = new ApplicationDbContext(nameOrConnString);
		}
		public IExternalLoginRepository ExternalLoginRepository
		{
			get
			{
				return _externalLoginRepository ?? (_externalLoginRepository = new ExternalLoginRepository(_cxt));
			}
		}

		public IRoleRepository RoleRepository
		{
			get
			{
				return _roleRepository ?? (_roleRepository = new RoleRepository(_cxt));
			}
		}

		public IUserRepository UserRepository
		{
			get
			{
				return _userRepository ?? (_userRepository = new UserRepository(_cxt));
			}
		}
		public int SaveChanges()
		{
			return _cxt.SaveChanges();
		}

		public Task<int> SaveChangesAsync()
		{
			return _cxt.SaveChangesAsync();
		}

		public Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
		{
			return _cxt.SaveChangesAsync(cancellationToken);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					_cxt.Dispose();
				}
			}
			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
