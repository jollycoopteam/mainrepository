﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Backend.Data.EF.Abstractions.Repos;
using Backend.Domain.Models;

namespace Backend.Data.EF.Implementation.Repos
{
	public class ExternalLoginRepository : BaseGenericRepository<ExternalLogin>, IExternalLoginRepository
	{
		public ExternalLoginRepository(ApplicationDbContext ctx) : base(ctx){}
		public ExternalLogin GetByProviderAndKey(string loginProvider, string providerKey)
		{
			return Set.FirstOrDefault(x => x.LoginProvider == loginProvider && x.ProviderKey == providerKey);
		}

		public Task<ExternalLogin> GetByProviderAndKeyAsync(string loginProvider, string providerKey)
		{
			return Set.FirstOrDefaultAsync(x => x.LoginProvider == loginProvider && x.ProviderKey == providerKey);
		}

		public Task<ExternalLogin> GetByProviderAndKeyAsync(CancellationToken cancellationToken, string loginProvider, string providerKey)
		{
			return Set.FirstOrDefaultAsync(x => x.LoginProvider == loginProvider && x.ProviderKey == providerKey, cancellationToken);
		}
	}
}
