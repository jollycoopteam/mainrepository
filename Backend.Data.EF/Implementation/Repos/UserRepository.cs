﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Backend.Data.EF.Abstractions.Repos;
using Backend.Domain.Models;

namespace Backend.Data.EF.Implementation.Repos
{
	public class UserRepository : BaseGenericRepository<User>, IUserRepository
	{
		public UserRepository(ApplicationDbContext ctx) : base(ctx)
		{
		}
		public User FindByUserName(string username)
		{
			return Set.FirstOrDefault(x => x.UserName == username);
		}

		public Task<User> FindByUserNameAsync(string username)
		{
			return Set.FirstOrDefaultAsync(x => x.UserName == username);
		}

		public Task<User> FindByUserNameAsync(CancellationToken cancellationToken, string username)
		{
			return Set.FirstOrDefaultAsync(x => x.UserName == username, cancellationToken);
		}

		public User FindByEmail(string email)
		{
			return Set.FirstOrDefault(x => x.Email == email);
		}

		public Task<User> FindByEmailAsync(string email)
		{
			return Set.FirstOrDefaultAsync(x => x.Email == email);
		}

		public Task<User> FindByEmailAsync(CancellationToken cancellationToken, string email)
		{
			return Set.FirstOrDefaultAsync(x => x.Email == email, cancellationToken);
		}
	}
}
