﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Backend.Data.EF.Abstractions.Repos;

namespace Backend.Data.EF.Implementation.Repos
{
	public class BaseGenericRepository<TEntity> : IGenericRepository<TEntity, Guid> where TEntity : class
	{
		private readonly ApplicationDbContext _ctx;
		private DbSet<TEntity> _set;

		public BaseGenericRepository(ApplicationDbContext ctx)
		{
			_ctx = ctx;
		}

		protected DbSet<TEntity> Set
		{
			// ReSharper disable once ConvertPropertyToExpressionBody
			get { return _set ?? (_set = _ctx.Set<TEntity>()); }
		}

		public List<TEntity> GetAll()
		{
			return Set.ToList();
		}

		public Task<List<TEntity>> GetAllAsync()
		{
			return Set.ToListAsync();
		}

		public Task<List<TEntity>> GetAllAsync(CancellationToken cancellationToken)
		{
			return Set.ToListAsync(cancellationToken);
		}

		public List<TEntity> PageAll(int skip, int take)
		{
			return Set.Skip(skip).Take(take).ToList();
		}

		public Task<List<TEntity>> PageAllAsync(int skip, int take)
		{
			return Set.Skip(skip).Take(take).ToListAsync();
		}

		public Task<List<TEntity>> PageAllAsync(CancellationToken cancellationToken, int skip, int take)
		{
			return Set.Skip(skip).Take(take).ToListAsync(cancellationToken);
		}

		public TEntity FindById(Guid id)
		{
			return Set.Find(id);
		}

		public Task<TEntity> FindByIdAsync(Guid id)
		{
			return Set.FindAsync(id);
		}

		public Task<TEntity> FindByIdAsync(CancellationToken cancellationToken, Guid id)
		{
			return Set.FindAsync(cancellationToken, id);
		}

		public void Add(TEntity entity)
		{
			Set.Add(entity);
		}

		public void Update(TEntity entity)
		{
			var entry = _ctx.Entry(entity);
			if (entry.State == EntityState.Detached)
			{
				Set.Attach(entity);
				entry = _ctx.Entry(entity);
			}
			entry.State = EntityState.Modified;
		}

		public void Remove(TEntity entity)
		{
			Set.Remove(entity);
		}
	}
}
