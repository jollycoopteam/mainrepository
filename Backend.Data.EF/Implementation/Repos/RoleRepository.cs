﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Backend.Data.EF.Abstractions.Repos;
using Backend.Domain.Models;

namespace Backend.Data.EF.Implementation.Repos
{
	public class RoleRepository : BaseGenericRepository<Role>, IRoleRepository
	{
		public RoleRepository(ApplicationDbContext ctx) : base(ctx){}
		public Role FindByName(string roleName)
		{
			return Set.FirstOrDefault(x => x.Name == roleName);
		}

		public Task<Role> FindByNameAsync(string roleName)
		{
			return Set.FirstOrDefaultAsync(x => x.Name == roleName);
		}

		public Task<Role> FindByNameAsync(CancellationToken cancellationToken, string roleName)
		{
			return Set.FirstOrDefaultAsync(x => x.Name == roleName, cancellationToken);
		}
	}
}
